package chatapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.ListIterator;

public class Server {

    private ArrayList<ServerClientSession> connections
            = (new ArrayList<ServerClientSession>());//список подключений
    private ServerSocket server;

    public Server() {
        try {
            server = new ServerSocket(5558);
            int number = 0;
            while (true) {
                Socket socket = server.accept();
                ServerClientSession curCon = new ServerClientSession(socket, number);
                connections.add(curCon);
                curCon.start();
                ++number;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
    }

    private void closeAll() {
        try {
            server.close();

            synchronized (connections) {
                ListIterator<ServerClientSession> iter = connections.listIterator();
                while (iter.hasNext()) {
                    ((ServerClientSession) iter.next()).close();
                }
            }
        } catch (Exception e) {
            System.err.println("Потоки не были закрыты!");
        }
    }

    private class ServerClientSession extends Thread {

        private BufferedReader in;
        private PrintWriter out;
        private Socket socket;
        private int number;
        private String name = "";

        public ServerClientSession(Socket socket, int numb) {
            this.socket = socket;
            this.number = numb;

            try {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);
            } catch (IOException e) {
                close();
            }
        }

        @Override
        public void run() {
            int currInd;
            try {
                name = in.readLine();
                //оповещение о новом пользователе
                synchronized (connections) {
                    ListIterator<ServerClientSession> iter = connections.listIterator();
                    while (iter.hasNext()) {
                        currInd = iter.nextIndex();
                        if (currInd != number) {
                            //currInd + " " + number + " "
                            ((ServerClientSession) iter.next()).out.println(name + " cames now");
                        } else {
                            iter.next();
                        }
                    }
                }

                String str = "";
                while (true) {
                    str = in.readLine();
                    if (str.equals("exit")) {
                        break;
                    }

                    // sendAll
                    synchronized (connections) {
                        ListIterator<ServerClientSession> iter = connections.listIterator();
                        while (iter.hasNext()) {
                            currInd = iter.nextIndex();
                            if (currInd != number) {
                                ((ServerClientSession) iter.next()).out.println(name + ": " + str);
                            } else {
                                iter.next();
                            }
                        }
                    }
                }
                //someone input "exit"
                synchronized (connections) {
                    ListIterator<ServerClientSession> iter = (ListIterator<ServerClientSession>) connections.listIterator();
                    while (iter.hasNext()) {
                        currInd = iter.nextIndex();
                        if (currInd != number) {
                            ((ServerClientSession) iter.next()).out.println(name + " has left");
                        } else {
                            iter.next();
                        }
                    }
                }
            } catch (IOException e) {
            } finally {
                close();
            }
        }

        public void close() {
            try {
                in.close();
                out.close();
                socket.close();

                connections.remove(this);
                if (connections.isEmpty()) {
                    Server.this.closeAll();
                    System.exit(0);
                }
            } catch (IOException e) {
                System.err.println("Потоки не были закрыты!");
            }
        }
    }
}
