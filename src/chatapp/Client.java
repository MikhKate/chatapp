package chatapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

public class Client {
    private BufferedReader in;
    private PrintWriter out;
    private Socket socket;
    private String name;

    public Client() {
        String host = "127.0.0.1";
        int port = 5558;
        Scanner scan = new Scanner(System.in);

        try {
            socket = new Socket(host, port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            
            System.out.println("Input your name: ");
            name = scan.nextLine();
            out.println(name);
            
            //run observer
            ClientSocketObserver sender = new ClientSocketObserver();
            sender.start();

            String str = "";

            while (!str.equals("exit")) {
                str = scan.nextLine();
                out.println(str);
            }
            sender.setStop();
        } catch (Exception e) {
        } finally {
            close();
        }
    }

    private void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (Exception e) {
            System.err.println("Потоки не были закрыты!");
        }
    }

    private class ClientSocketObserver extends Thread {

        private boolean stoped;
        public void setStop() {
            stoped = true;
        }

        @Override
        public void run() {
            try {
                while (!stoped) {
                    String str = in.readLine();
                    System.out.println(str);
                }
            } catch (IOException e) {
                System.err.println("Ошибка при получении сообщения.");
            }
        }
    }
}
